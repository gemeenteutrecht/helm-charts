# About
This repository contains various Helm charts which have been initiated by Gemeente Utrecht. Please be aware that these open-source charts are a community effort, therefore no rights can be derived from them. 

Details for each chart can be found in the README of the concerning chart. 

# Adding a new chart
New charts can be added to the `charts/` directory. Afterwards use the following `make` command in the root of this repo to generate the required pipeline-config:
```
make create-pipeline CHART=demo-app
```
where the argument for the CHART-parameter should be the name of the chart as you added it to the `charts/` folder. This command will generate a pipeline-configuration for this chart in the `.gitlab` directory. 

## Subdirectories
In case you have multiple charts in a subdirectory withinin `charts/`, you will need to specify this when running the `make` command. Consider the following lay-out:
```
charts
└── foo
    ├── bar
    └── baz
```

In this case you will need to use the `CHARTPATH` argument as follows:
```
 make create-pipeline CHART=bar CHARTPATH=charts/foo
```
Execute again but now for the `baz` chart:
```
 make create-pipeline CHART=baz CHARTPATH=charts/foo
```

> if you ommit the `CHARTPATH` argument, the CHARTPATH variable in the pipeline will default to `charts`

## Chart documentation
Please use [helm-docs](https://github.com/norwoodj/helm-docs) to generate chart documentation in the chart's README.md. Make sure to document each helm-value like this:
```
# -- This comment documents foo
foo: bar
```
Installation instructions will need to be added manually to the generated README.md. See https://gitlab.com/gemeenteutrecht/helm-charts/-/blob/main/charts/bptl/README.md#installation for an example.
> Also make sure to re-run the `helm-docs` command when you change an existing chart, e.g. add a new helm-value. You will also need to re-add the installation instructions mentioned above.

# Contributing
Contributions to these charts are welcome! If you want to contribute, then please fork this project and submit a merge-request. 