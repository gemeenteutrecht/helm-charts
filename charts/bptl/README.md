# bptl

![Version: 0.2.1](https://img.shields.io/badge/Version-0.2.1-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | postgresql | ~10.12.0 |
| https://charts.bitnami.com/bitnami | redis | ~13.0.0 |

## Installation

First configure the Helm repository:

```bash
helm repo add gemeente-utrecht https://gitlab.com/api/v4/projects/34434173/packages/helm/stable
helm repo update
```

Install the Helm chart with:

```bash
helm install bptl gemeente-utrecht/bptl \
    --set "settings.allowedHosts=bptl.gemeente.nl" \
    --set "ingress.enabled=true" \
    --set "ingress.hosts=bptl.gemeente.nl"
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for BPTL pods assignment  |
| autoscaling.enabled | bool | `false` | Enable/disable autoscaling for the BPTL deployment |
| autoscaling.maxReplicas | int | `100` | Maximum replicas for the BPTL deployment |
| autoscaling.minReplicas | int | `1` | Minimum replicas for the BPTL deployment |
| autoscaling.targetCPUUtilizationPercentage | int | `80` | BPTL Deployment autoscaling target CPU percentage |
| autoscaling.targetMemoryUtilizationPercentage | int | `80` | BPTL Deployment autoscaling target Mem utilization percentage |
| beat.autoscaling.enabled | bool | `false` | Enable/disable autoscaling for Celery Beat. Keep at 'false' unless you know what you're doing! |
| beat.autoscaling.maxReplicas | int | `2` | Maximum amount of replicas for Celery Beat |
| beat.autoscaling.minReplicas | int | `1` | Minimum amount of replicas for Celery Beat |
| beat.autoscaling.targetCPUUtilizationPercentage | int | `80` | Celery Beat deployment autoscaling target CPU percentage |
| beat.autoscaling.targetMemoryUtilizationPercentage | int | `80` | Celery Beat deployment autoscaling target Mem utilization percentage |
| beat.podAnnotations | object | `{}` | Annotations for the Celery Beat pods |
| beat.podLabels | object | `{}` | Set additional labels on the Beat pods |
| beat.replicaCount | int | `1` | Amount of replica's for the Celery Beat component. Keep at '1' unless you know what you're doing! |
| beat.resources | object | `{}` | Celery Beat resource configuration |
| existingSecret | string | `nil` | Refer to an existing secret to avoid managing secrets through Helm. |
| flower.enabled | bool | `true` | Enable Celery Flower |
| flower.extraEnvVars | object | `{}` | Set addtional Flower configuration. See [Flower docs](https://flower.readthedocs.io/en/latest/config.html) for more info. |
| flower.extraEnvVarsSecret | object | `{}` | Set addtional Flower secrets. See [Flower docs](https://flower.readthedocs.io/en/latest/config.html) for more info. |
| flower.failureThreshold | int | `6` | Failure threshold for Celery Flower livenessProbe |
| flower.ingress.annotations | object | `{}` | Ingress annotations |
| flower.ingress.className | string | `""` | Ingress Class which will be used to implement the Ingress |
| flower.ingress.enabled | bool | `false` | Expose the Celery Flower UI through an ingress |
| flower.ingress.hosts[0] | object | `{"host":"chart-example.local","paths":[{"path":"/","pathType":"ImplementationSpecific"}]}` | Celery Flower Ingress hostname |
| flower.ingress.hosts[0].paths[0] | object | `{"path":"/","pathType":"ImplementationSpecific"}` | Celery Flower Ingress path |
| flower.ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` | Celery Flower Ingress path type |
| flower.ingress.tls | list | `[]` | Enable TLS for the Celery Flower Ingress |
| flower.initialDelaySeconds | int | `60` | Initial delay seconds for Celery Flower livenessProbe |
| flower.livenessProbe | string | `nil` |  |
| flower.periodSeconds | int | `10` | Period seconds for Celery Flower livenessProbe |
| flower.podAnnotations | object | `{}` | Set additional annotations on the Celery Flower pods |
| flower.podLabels | object | `{}` | Set additional labels on the Celery Flower pods |
| flower.readinessProbe.failureThreshold | int | `6` | Failure threshold for Celery Flower readinessProbe |
| flower.readinessProbe.initialDelaySeconds | int | `30` | Initial delay seconds for Celery Flower readinessProbe |
| flower.readinessProbe.periodSeconds | int | `10` | Period seconds for Celery Flower readinessProbe |
| flower.readinessProbe.successThreshold | int | `1` | Success threshold for Celery Flower readinessProbe |
| flower.readinessProbe.timeoutSeconds | int | `5` | Timeout seconds for Celery Flower readinessProbe |
| flower.replicaCount | int | `1` | Amount of replica's for the Celery Flower component |
| flower.resources | object | `{}` | Resources for Celery Flower |
| flower.service.port | int | `80` | Celery Flower service port |
| flower.service.type | string | `"ClusterIP"` | Celery Flower service type |
| flower.successThreshold | int | `1` | Success threshold for Celery Flower livenessProbe |
| flower.timeoutSeconds | int | `5` | Timeout seconds for Celery Flower livenessProbe |
| fullnameOverride | string | `""` | String to fully override bptl.fullname |
| image.pullPolicy | string | `"IfNotPresent"` | BPTL image pull policy |
| image.repository | string | `"scrumteamzgw/bptl"` | BPTL image repository |
| image.tag | string | `""` | Image tag override (whose default is the chart appVersion) |
| imagePullSecrets | list | `[]` | Image pull secrets |
| ingress.annotations | object | `{}` | Ingress annotations |
| ingress.className | string | `""` | Ingress Class which will be used to implement the Ingress |
| ingress.enabled | bool | `false` | Expose the BPTL UI through an ingress |
| ingress.hosts[0] | object | `{"host":"chart-example.local","paths":[{"path":"/","pathType":"ImplementationSpecific"}]}` | Ingress hostname |
| ingress.hosts[0].paths[0] | object | `{"path":"/","pathType":"ImplementationSpecific"}` | Ingress path |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` | Ingress path type |
| ingress.tls | list | `[]` | Enable TLS for the Ingress |
| livenessProbe.failureThreshold | int | `4` | Failure threshold for livenessProbe |
| livenessProbe.initialDelaySeconds | int | `60` | Initial delay seconds for livenessProbe |
| livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| livenessProbe.timeoutSeconds | int | `1` | Timeout seconds for livenessProbe |
| longPollingWorker.autoscaling.enabled | bool | `false` | Enable/disable autoscaling for Celery longpoller |
| longPollingWorker.autoscaling.maxReplicas | int | `100` | Maximum amount of replicas for Celery longpoller |
| longPollingWorker.autoscaling.minReplicas | int | `1` | Minimum amount of replicas for Celery longpoller |
| longPollingWorker.autoscaling.targetCPUUtilizationPercentage | int | `80` | Celery longpoller deployment autoscaling target CPU percentage |
| longPollingWorker.autoscaling.targetMemoryUtilizationPercentage | int | `80` | Celery longpoller deployment autoscaling target Mem utilization percentage |
| longPollingWorker.podAnnotations | object | `{}` | Annotations for the Celery longpoller pods |
| longPollingWorker.podLabels | object | `{}` | Set additional labels on the longpoller pods |
| longPollingWorker.replicaCount | int | `1` | Amount of replica's for the Celery longpoller component |
| longPollingWorker.resources | object | `{}` | Celery longpollers resource configuration |
| nameOverride | string | `""` | Name override for BPTL |
| nodeSelector | object | `{}` | Node labels for BPTL pods assignment |
| podAnnotations | object | `{}` | Annotations for BPTL pods |
| podLabels | object | `{}` | Labels for BPTL pods |
| podSecurityContext.fsGroup | int | `2000` | Set BPTL's pod security fsGroup |
| postgresql.persistence.enabled | bool | `false` | Enable persistance through volumes for in-cluster postgres instance |
| postgresql.persistence.existingClaim | string | `nil` | Refer to an existing PVC for the in-cluster postgres instance |
| postgresql.persistence.size | string | `"1Gi"` | Size of the volume for the in-cluster postgres instance  |
| postgresql.postgresqlDatabase | string | `"bptl"` | In-cluster postgres database name |
| postgresql.postgresqlPassword | string | `"bptl"` | In-cluster postgres password |
| postgresql.postgresqlUsername | string | `"bptl"` | In-cluster postgres user name |
| readinessProbe.failureThreshold | int | `4` | Failure threshold for readinessProbe |
| readinessProbe.initialDelaySeconds | int | `30` | Initial delay seconds for readinessProbe |
| readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| readinessProbe.timeoutSeconds | int | `1` | Timeout seconds for readinessProbe |
| redis.cluster.enabled | bool | `false` | Run Redis in master-slave topology |
| redis.master.persistence.enabled | bool | `false` | Use a PVC to persist data (master node) |
| redis.master.persistence.size | string | `"1Gi"` | Size of the data-volume |
| redis.persistence.existingClaim | string | `nil` | Use an existing PVC for Redis persistancy |
| redis.usePassword | bool | `false` | Use a password as authentication for the in-cluster Redis instance  |
| replicaCount | int | `1` | Amount of replicas |
| resources | object | `{}` | Resources for BPTL |
| securityContext.capabilities.drop | list | `["ALL"]` | BPTL's container security context capabilities to be dropped |
| securityContext.readOnlyRootFilesystem | bool | `false` | BPTL's container security context readOnlyRootFilesystem |
| securityContext.runAsNonRoot | bool | `true` | Run BPTL containers as non-root |
| securityContext.runAsUser | int | `1000` | Run BPTL containers under this user-ID |
| service.port | int | `80` | BPTL service port |
| service.type | string | `"ClusterIP"` | BPTL service type |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. |
| settings.allowedHosts | string | `"localhost,bptl"` | Only requests originating from the sources in this comma-separated list have access |
| settings.cache.axes | string | `"bptl-redis-master:6379/0"` | Redis address for the django-axes cache |
| settings.cache.default | string | `"bptl-redis-master:6379/0"` | Redis address for the default cache |
| settings.celery.brokerUrl | string | `"redis://bptl-redis-master:6379/1"` | Redis' broker URL for Celery |
| settings.celery.resultBackend | string | `"redis://bptl-redis-master:6379/1"` | Redis' result store for Celery |
| settings.database.host | string | `"bptl-postgres"` | Hostname of the postgres database |
| settings.database.name | string | `"bptl"` | Name of the postgres database |
| settings.database.password | string | `"SUPERSECRET"` | Password to use for the postgres connection |
| settings.database.port | int | `5432` | Port number of the postgres database |
| settings.database.sslmode | string | `"prefer"` | SSL-mode for the postgres connection |
| settings.database.username | string | `"bptl"` | User name to use for the postgres connection |
| settings.djangoSettingsModule | string | `"bptl.conf.docker"` | The Django Settings module to use |
| settings.email.defaultFromMail | string | `""` | Default from-address for sent emails |
| settings.email.host | string | `"localhost"` | SMTP host to be used to send mail notifications |
| settings.email.password | string | `""` | SMTP password to be used to send mail notifications |
| settings.email.port | int | `25` | SMTP port to be used to send mail notifications |
| settings.email.useTLS | string | `"True"` | Use TLS for the SMTP connection |
| settings.email.username | string | `""` | SMTP user name to be used to send mail notifications |
| settings.flower.urlPrefix | string | `""` | Flower will listen on this configured prefix.  |
| settings.secretKey | string | `""` | The key for securing signed data - if you are managing this through Helm, then make sure to encrypt it!  |
| settings.sentry.dsn | string | `""` | Sentry DSN |
| tags.postgresql | bool | `true` | Install the Postgres subchart |
| tags.redis | bool | `true` | Install the Redis subchart |
| tolerations | list | `[]` | Tolerations for BPTL pods assignment |
| worker.autoscaling.enabled | bool | `false` | Enable/disable autoscaling for Celery Workers |
| worker.autoscaling.maxReplicas | int | `100` | Maximum amount of replicas for Celery Workers |
| worker.autoscaling.minReplicas | int | `1` | Minimum amount of replicas for Celery Workers |
| worker.autoscaling.targetCPUUtilizationPercentage | int | `80` | Celery Workers deployment autoscaling target CPU percentage |
| worker.autoscaling.targetMemoryUtilizationPercentage | int | `80` | Celery Workers deployment autoscaling target Mem utilization percentage |
| worker.podAnnotations | object | `{}` | Annotations for the Celery Worker pods |
| worker.podLabels | object | `{}` | Set additional labels on the Worker pods |
| worker.replicaCount | int | `1` | Amount of replica's for the Celery worker component |
| worker.resources | object | `{}` | Celery Workers resource configuration |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.0](https://github.com/norwoodj/helm-docs/releases/v1.11.0)
