{{/*
Expand the name of the chart.
*/}}
{{- define "bptl.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "bptl.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "bptl.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Labels
*/}}
{{- define "bptl.labels" -}}
{{ include "bptl.commonLabels" . }}
{{ include "bptl.selectorLabels" . }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "bptl.commonLabels" -}}
helm.sh/chart: {{ include "bptl.chart" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "bptl.selectorLabels" -}}
app.kubernetes.io/name: {{ include "bptl.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "bptl.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "bptl.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

#
## BPTL-CELERY-BEAT
#
{{/*
Create a name for Beat
We truncate at 56 chars in order to provide space for the "-beat" suffix
*/}}
{{- define "bptl.beatName" -}}
{{ include "bptl.name" . | trunc 57 | trimSuffix "-" }}-beat
{{- end }}

{{/*
Create a default fully qualified name for Beat.
We truncate at 56 chars in order to provide space for the "-beat" suffix
*/}}
{{- define "bptl.beatFullname" -}}
{{ include "bptl.fullname" . | trunc 57 | trimSuffix "-" }}-beat
{{- end }}

{{/*
Beat labels
*/}}
{{- define "bptl.beatLabels" -}}
{{ include "bptl.commonLabels" . }}
{{ include "bptl.beatSelectorLabels" . }}
{{- end }}

{{/*
Beat selector labels
*/}}
{{- define "bptl.beatSelectorLabels" -}}
app.kubernetes.io/name: {{ include "bptl.beatName" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

#
## BPTL-CELERY-WORKER
#
{{/*
Create a name for Worker
We truncate at 56 chars in order to provide space for the "-beat" suffix
*/}}
{{- define "bptl.workerName" -}}
{{ include "bptl.name" . | trunc 57 | trimSuffix "-" }}-worker
{{- end }}

{{/*
Create a default fully qualified name for Worker.
We truncate at 56 chars in order to provide space for the "-worker" suffix
*/}}
{{- define "bptl.workerFullname" -}}
{{ include "bptl.fullname" . | trunc 57 | trimSuffix "-" }}-worker
{{- end }}

{{/*
Worker labels
*/}}
{{- define "bptl.workerLabels" -}}
{{ include "bptl.commonLabels" . }}
{{ include "bptl.workerSelectorLabels" . }}
{{- end }}

{{/*
Worker selector labels
*/}}
{{- define "bptl.workerSelectorLabels" -}}
app.kubernetes.io/name: {{ include "bptl.workerName" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

#
## BPTL-CELERY-LONGPPOLLING-WORKER
#
{{/*
Create a name for Longpolling-Worker
We truncate at 56 chars in order to provide space for the "-longpolling-worker" suffix
*/}}
{{- define "bptl.longPollingWorkerName" -}}
{{ include "bptl.name" . | trunc 57 | trimSuffix "-" }}-longpolling-worker
{{- end }}

{{/*
Create a default fully qualified name for Longpolling-Worker.
We truncate at 56 chars in order to provide space for the "-longpolling-worker" suffix
*/}}
{{- define "bptl.longPollingWorkerFullname" -}}
{{ include "bptl.fullname" . | trunc 57 | trimSuffix "-" }}-longpolling-worker
{{- end }}

{{/*
Longpolling-Worker labels
*/}}
{{- define "bptl.longPollingWorkerLabels" -}}
{{ include "bptl.commonLabels" . }}
{{ include "bptl.longPollingWorkerSelectorLabels" . }}
{{- end }}

{{/*
Longpolling-Worker selector labels
*/}}
{{- define "bptl.longPollingWorkerSelectorLabels" -}}
app.kubernetes.io/name: {{ include "bptl.longPollingWorkerName" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

#
## BPTL-CELERY-FLOWER
#
{{/*
Create a name for Flower
We truncate at 56 chars in order to provide space for the "-flower" suffix
*/}}
{{- define "bptl.flowerName" -}}
{{ include "bptl.name" . | trunc 56 | trimSuffix "-" }}-flower
{{- end }}

{{/*
Create a default fully qualified name for Flower.
We truncate at 56 chars in order to provide space for the "-flower" suffix
*/}}
{{- define "bptl.flowerFullname" -}}
{{ include "bptl.fullname" . | trunc 56 | trimSuffix "-" }}-flower
{{- end }}

{{/*
Flower labels
*/}}
{{- define "bptl.flowerLabels" -}}
{{ include "bptl.commonLabels" . }}
{{ include "bptl.flowerSelectorLabels" . }}
{{- end }}

{{/*
Flower selector labels
*/}}
{{- define "bptl.flowerSelectorLabels" -}}
app.kubernetes.io/name: {{ include "bptl.flowerName" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}