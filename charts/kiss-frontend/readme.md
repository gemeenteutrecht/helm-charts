# kiss-frontend

![Version: 1.0.56](https://img.shields.io/badge/Version-1.0.56-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.0.0](https://img.shields.io/badge/AppVersion-1.0.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].key | string | `"app"` |  |
| affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].operator | string | `"In"` |  |
| affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].values[0] | string | `"kiss-frontend"` |  |
| affinity.podAntiAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].topologyKey | string | `"kubernetes.io/hostname"` |  |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.maxReplicas | int | `100` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| config.AFDELINGEN_BASE_URL | string | "" |  |
| config.AFDELINGEN_OBJECT_TYPE_URL | string | "" |  |
| config.ASPNETCORE_ENVIRONMENT | string | "" |  |
| config.CONTACTMOMENTEN_BASE_URL | string | "" |  |
| config.CONTACT_API_CLIENT_ID | string | "" |  |
| config.ELASTIC_BASE_URL | string | "" |  |
| config.ELASTIC_USERNAME | string | "" |  |
| config.EMAIL_ENABLE_SSL | string | "" |  |
| config.EMAIL_HOST | string | "" |  |
| config.EMAIL_PORT | int | `25` |  |
| config.EMAIL_USERNAME | string | "" |  |
| config.ENTERPRISE_SEARCH_BASE_URL | string | "" |  |
| config.ENTERPRISE_SEARCH_ENGINE | string | "" |  |
| config.FEEDBACK_EMAIL_FROM | string | "" |  |
| config.FEEDBACK_EMAIL_TO | string | "" |  |
| config.GROEPEN_BASE_URL | string | "" |  |
| config.GROEPEN_OBJECT_TYPE_URL | string | "" |  |
| config.HAAL_CENTRAAL_BASE_URL | string | "" |  |
| config.INTERNE_TAAK_OBJECT_TYPE_URL | string | "" |  |
| config.INTERNE_TAAK_BASE_URL | string | "" |  |
| config.INTERNE_TAAK_TYPE_VERSION | string | "" |  |
| config.KLANTEN_BASE_URL | string | "" |  |
| config.KLANTEN_CLIENT_ID | string | "" |  |
| config.KVK_BASE_URL | string | "" |  |
| config.MW_OBJECTEN_BASE_URL | string | "" |  |
| config.MW_OBJECTTYPES_BASE_URL | string | "" |  |
| config.OIDC_AUTHORITY | string | "" |  |
| config.OIDC_CLAIM | string | "" |  |
| config.OIDC_CLIENT_ID | string | "" |  |
| config.OIDC_TRUNCATE | int | `24` |  |
| config.ORGANISATIE_IDS | string | "" |  |
| config.POSTGRES_DB | string | "" |  |
| config.POSTGRES_HOST | string | "" |  |
| config.POSTGRES_PORT | int | `5432`  |  |
| config.POSTGRES_USER | string | "" |  |
| config.SDG_BASE_URL | string | "" |  |
| config.SDG_OBJECTEN_BASE_URL | string | "" |  |
| config.SDG_OBJECT_TYPE_URL | string | "" |  |
| config.VAC_OBJECTEN_BASE_URL | string | "" |  |
| config.VAC_OBJECTTYPES_BASE_URL | string | "" |  |
| config.ZAAKSYSTEEM_DEEPLINK_URL | string | "" |  |
| config.ZAAKSYSTEEM_PROPERTY | string | "" |  |
| config.ZAKEN_API_CLIENT_ID | string | "" |  |
| config.ZAKEN_BASE_URL | string | "" |  |
| cronjobs | list | `[]` |  |
| env.config.ASPNETCORE_ENVIRONMENT | string | `"Production"` |  |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"ghcr.io/klantinteractie-servicesysteem/kiss-frontend"` |  |
| image.tag | string | `"latest"` |  |
| imagePullSecrets | list | `[]` |  |
| ingress.annotations | object | `{}` |  |
| ingress.className | string | `"nginx"` |  |
| ingress.enabled | bool | `false` |  |
| ingress.hosts[0].host | string | `"chart-example.local"` |  |
| ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| ingress.hosts[0].paths[0].serviceName | string | "" |  |
| ingress.hosts[0].paths[0].servicePort | string | "" |  |
| ingress.tls | list | `[]` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| postgresql.enabled | bool | `true` |  |
| postgresql.service.port | int | `5432` |  |
| replicaCount | int | `2` |  |
| resources | object | `{}` |  |
| secretConfig.afdelingenToken | string | "" |  |
| secretConfig.contactmomentenApiKey | string | "" |  |
| secretConfig.elasticPassword | string | "" |  |
| secretConfig.emailPassword | string | "" |  |
| secretConfig.enterpriseSearchPrivateApiKey | string | "" |  |
| secretConfig.enterpriseSearchPublicApiKey | string | "" |  |
| secretConfig.groepenToken | string | "" |  |
| secretConfig.haalCentraalApiKey | string | "" |  |
| secretConfig.interneTaakToken | string | "" |  |
| secretConfig.klantenClientSecret | string | "" |  |
| secretConfig.kvkApiKey | string | "" |  |
| secretConfig.medewerkerObjectenToken | string | "" |  |
| secretConfig.medewerkerObjecttypesToken | string | "" |  |
| secretConfig.oidcClientSecret | string | "" |  |
| secretConfig.postgresPassword | string | "" |  |
| secretConfig.sdgObjectenToken | string | "" |  |
| secretConfig.secretName | string | `"kiss-secrets"` |  |
| secretConfig.vacObjectenToken | string | "" |  |
| secretConfig.vacObjecttypesToken | string | "" |  |
| secretConfig.zakenApiKey | string | "" |  |
| securityContext | object | `{}` |  |
| service.port | int | `8080` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `false` |  |
| serviceAccount.name | string | `""` |  |
| strategy.rollingUpdate.maxSurge | int | `1` |  |
| strategy.rollingUpdate.maxUnavailable | int | `0` |  |
| strategy.type | string | `"RollingUpdate"` |  |
| tolerations | list | `[]` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.13.1](https://github.com/norwoodj/helm-docs/releases/v1.13.1)
