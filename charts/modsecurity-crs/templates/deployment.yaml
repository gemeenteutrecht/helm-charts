apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "modsecurity-crs.fullname" . }}
  labels:
    {{- include "modsecurity-crs.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "modsecurity-crs.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      annotations:
        checksum/configmap: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
        checksum/secret: {{ include (print $.Template.BasePath "/secret.yaml") . | sha256sum }}
        {{- with .Values.podAnnotations }}
          {{- toYaml . | nindent 8 }}
        {{- end }}
      labels:
        {{- include "modsecurity-crs.selectorLabels" . | nindent 8 }}
        {{- with .Values.podLabels }}
            {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "modsecurity-crs.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          envFrom:
            - secretRef:
                name: {{ .Values.existingSecret | default (include "modsecurity-crs.fullname" .) }}
            - configMapRef:
                name: {{ include "modsecurity-crs.fullname" . }}
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
            {{- toYaml .Values.livenessProbe | nindent 12 }}
          readinessProbe:
            httpGet:
              path: /
              port: http
            {{- toYaml .Values.readinessProbe | nindent 12 }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}  
          volumeMounts:
            - mountPath: /var/log/modsecurity/
              name: modsecurity-logs
            - mountPath: /var/log/nginx/
              name: nginx-logs
            - name: modsec-config
              mountPath: /etc/nginx/templates/conf.d/logging.conf.template
              subPath: nginx-accesslog-config
            - name: modsec-config
              mountPath: /etc/modsecurity.d/owasp-crs/rules/RESPONSE-999-EXCLUSION-RULES-AFTER-CRS.conf
              subPath: exclusion-rules-after-crs
      {{- if .Values.filebeat.enabled }}
        - name: {{ .Chart.Name }}-filebeat
          securityContext:
            {{- toYaml .Values.filebeat.securityContext | nindent 12 }}
          image: "{{ .Values.filebeat.image.repository }}:{{ .Values.filebeat.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.filebeat.image.pullPolicy }}
          envFrom:
            - configMapRef:
                name: {{ include "modsecurity-crs.fullname" . }}-filebeat
          resources:
            {{- toYaml .Values.filebeat.resources | nindent 12 }}
          volumeMounts: 
            - mountPath: /var/log/modsecurity
              name: modsecurity-logs
            - mountPath: /var/log/nginx
              name: nginx-logs
            - mountPath: /usr/share/filebeat/filebeat.yml
              name: filebeat-config
              subPath: filebeat.yml
      {{- end }}
        - name: {{ .Chart.Name }}-echoserver
          securityContext:
            {{- toYaml .Values.echoserver.securityContext | nindent 12 }}
          image: "{{ .Values.echoserver.image.repository }}:{{ .Values.echoserver.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.echoserver.image.pullPolicy }}
          ports:
          - containerPort: 8081
          resources:
            {{- toYaml .Values.echoserver.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      volumes:
        - name: modsec-config
          configMap:
            name: {{ include "modsecurity-crs.fullname" . }}
        - name: modsecurity-logs
        - name: nginx-logs
        - name: filebeat-config
          configMap:
            items:
              - key: filebeat.yml
                path: filebeat.yml
            name: {{ template "modsecurity-crs.fullname" . }}-filebeat
