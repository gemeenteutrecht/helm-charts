# Modsecurity-CRS Helm Chart
In a solution like ingress-nginx it is possible to directly use modsecurity. For other ingress gateway solutions like Istio this is not possible. This opiniated Helm Chart caters for the latter and has been validated with Istio Ingress Gateway:

![Screenshot](./docs/istio_modsec.png) 

## Deployment
This deployment runs three containers:
### 1. [modsecurity-crs](https://github.com/coreruleset/modsecurity-crs-docker)  
See https://github.com/coreruleset/modsecurity-crs-docker for details and check out `values.yaml` for a config example. 
  
### 2. echoserver  
Modsecurity-crs can only be configured in proxy mode, which means it expects a backend to forward the traffic to. In this case, an Ingress solutions like Istio Ingress Gateway is responsible for forwarding traffic further into the cluster. Therefore, the echoserver container is configured as a backend; this server should always return an HTTP 200. 

### 3. filebeat (optional)
In case you want to ship modsec logs to an external system, e.g. a SIEM, you can enable the Filebeat container to do so. Currently this chart only supports Filebeat as logshipper. A generic method for adding arbitrary containers might be added in the future. 

## Filters
The decision whether to forward traffic further into the cluster is the responsibility of the Gateway solution and should be done based on the response of `modsecurity-crs`. For Istio Ingress Gateway, the following `EnvoyFilter` can be used for this: 

```
apiVersion: networking.istio.io/v1alpha3
kind: EnvoyFilter
metadata:
  name: ext-authz-http
  namespace: istio-ingress
spec:
  workloadSelector:
    # Apply this filter to all workloads in the mesh.
    labels:
      istio.io/gateway-name: public-gateway
  configPatches:
    # The first patch adds the external authorization filter to the filter chain.
    - applyTo: HTTP_FILTER
      match:
        context: GATEWAY
        listener:
          filterChain:
            filter:
              name: "envoy.filters.network.http_connection_manager"
              subFilter:
                name: "envoy.filters.http.router"
      patch:
        operation: INSERT_BEFORE
        value:
          name: envoy.filters.http.ext_authz
          typed_config:
            "@type": type.googleapis.com/envoy.extensions.filters.http.ext_authz.v3.ExtAuthz
            http_service:
              server_uri:
                uri: "http://modsecurity-crs-service.modsec.svc.cluster.local:80"
                cluster: "outbound|80||modsecurity-crs-service.modsec.svc.cluster.local"
                timeout: 0.5s
              authorization_request:
                allowed_headers:
                  patterns:
                    - exact: "authorization"
                    - exact: "cookie"
                    - exact: "x-forwarded-for"
                    - exact: "x-request-id"
                    - exact: "x-envoy-original-path"
              authorization_response:
                allowed_upstream_headers:
                  patterns:
                    - exact: "x-ext-authz-check"
                allowed_client_headers:
                  patterns:
                    - exact: "x-ext-authz-check"
    # The second patch ensures that the `modsecurity-crs-service` cluster is added to Envoy's cluster manager.
    - applyTo: CLUSTER
      match:
        cluster:
          service: "modsecurity-crs-service.modsec.svc.cluster.local"
      patch:
        operation: ADD
        value:
          name: "outbound|80||modsecurity-crs-service.modsec.svc.cluster.local"
          type: STRICT_DNS
          connect_timeout: 0.25s
          lb_policy: ROUND_ROBIN
          load_assignment:
            cluster_name: "outbound|80||modsecurity-crs-service.modsec.svc.cluster.local"
            endpoints:
              - lb_endpoints:
                  - endpoint:
                      address:
                        socket_address:
                          address: "modsecurity-crs-service.modsec.svc.cluster.local"
                          port_value: 80

```