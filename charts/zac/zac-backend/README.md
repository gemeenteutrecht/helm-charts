# zac-backend

![Version: 0.1.1](https://img.shields.io/badge/Version-0.1.1-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.44.0](https://img.shields.io/badge/AppVersion-0.44.0-informational?style=flat-square)

A Helm chart to deploy zaakafhandelcomponent backend resources

**Homepage:** <https://github.com/GemeenteUtrecht/zaakafhandelcomponent>

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | elasticsearch | 17.x.x |
| https://charts.bitnami.com/bitnami | redis | ~13.0.0 |

## Installation

First configure the Helm repository:

```bash
helm repo add gemeente-utrecht https://gitlab.com/api/v4/projects/34434173/packages/helm/stable
helm repo update
```

Install the Helm chart with:

```bash
helm install zac-backend gemeente-utrecht/zac-backend \
    --set "settings.allowedHosts=zac-backend.gemeente.nl" \
    --set "ingress.enabled=true" \
    --set "ingress.hosts=zac-backend.gemeente.nl"
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for zac-backend pods assignment  |
| allowedHosts | object | `{}` | Comma seperated list of allowed hosts (e.g. "localhost,zac.default") |
| autoscaling.enabled | bool | `false` | Enable/disable autoscaling for the zac-backend deployment |
| autoscaling.maxReplicas | int | `100` | Maximum replicas for the zac-backend deployment |
| autoscaling.minReplicas | int | `1` | Minimum replicas for the zac-backend deployment |
| autoscaling.targetCPUUtilizationPercentage | int | `80` | zac-backend deployment autoscaling target CPU percentage |
| autoscaling.targetMemoryUtilizationPercentage | int | `80` | zac-backend deployment autoscaling target Mem utilization percentage |
| cache.axes | string | `""` | Redis address for the django-axes cache |
| cache.default | string | `""` | Redis address for the default cache |
| cache.oas | string | `""` | Redis address for the oas cache |
| database.host | string | `"postgresql"` | postgres host |
| database.name | string | `"postgresql"` | postgres database name |
| database.pgsslmode | string | `"require"` | postgres ssl-mode for the connection |
| database.port | int | `5432` | postgres port |
| database.user | string | `"psadmin"` | postgres user name |
| elasticApmServerUrl | string | `""` | URL of the elastic APM server instance |
| elasticsearch.enabled | bool | `false` | Enable the Elasticsearch subchart |
| email.from | string | `""` | outgoing mailserver mail-from  |
| email.host | string | `""` | outgoing mailserver |
| email.port | string | `""` | outgoing mailserver port |
| email.tls | bool | `true` | outgoing mailserver TLS enabled/disabled |
| email.user | string | `""` | outgoing mailserver user |
| environment | string | `"default"` | Environment in which zac-backend is running (development, staging, production) |
| esHost | string | `""` | ElasticSearch host |
| extraEnvs | list | `[]` | Additional environment variables to set |
| fullnameOverride | string | `""` | String to fully override zac-backend.fullname |
| image.pullPolicy | string | `"Always"` | ZAC-backend image pull policy |
| image.repository | string | `"scrumteamzgw/zaakafhandelcomponent"` | ZAC-backend image repository |
| image.tag | string | `""` | Image tag override (whose default is the chart appVersion) |
| imagePullSecrets | list | `[]` | Image pull secrets |
| ingress.annotations | object | `{}` | Ingress annotations |
| ingress.className | string | `""` | Ingress Class which will be used to implement the Ingress |
| ingress.enabled | bool | `false` | Expose the BPTL UI through an ingress |
| ingress.hosts[0] | object | `{"host":"chart-example.local","paths":[{"path":"/","pathType":"ImplementationSpecific"}]}` | Ingress hostname |
| ingress.hosts[0].paths[0] | object | `{"path":"/","pathType":"ImplementationSpecific"}` | Ingress path |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` | Ingress path type |
| ingress.tls | list | `[]` | Enable TLS for the Ingress |
| livenessProbe.enabled | bool | `true` | Enable/disable the livenessProbe |
| livenessProbe.failureThreshold | int | `2` | Failure threshold for livenessProbe |
| livenessProbe.httpGet.httpHeaders | list | `[]` | Additional headers to be sent by the livenessProbe |
| livenessProbe.httpGet.path | string | `"/healthz"` | Path target for the HTTP GET request of the livenessProbe |
| livenessProbe.httpGet.port | string | `"http"` | Port target for the HTTP GET request of the livenessProbe |
| livenessProbe.httpGet.scheme | string | `"HTTP"` | Scheme to be used for the HTTP GET request of the livenessProbe |
| livenessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for livenessProbe |
| livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| nameOverride | string | `""` | Name override for ZAC-backend |
| nodeSelector | object | `{}` | Node labels for zac-backend pods assignment |
| objectsApiSchema | string | `""` | URL for the Objects-API schema |
| objecttypesApiSchema | string | `""` | URL for the Objecttypes-API schema |
| podAnnotations | object | `{}` | Annotations for ZAC-backend pods |
| podLabels | object | `{}` | Labels for ZAC-backend pods |
| podSecurityContext | object | `{}` | Set zac-backend pod security context |
| readinessProbe.enabled | bool | `true` | Enable/disable the readinessProbe |
| readinessProbe.failureThreshold | int | `6` | Failure threshold for readinessProbe |
| readinessProbe.httpGet.httpHeaders | list | `[]` | Additional headers to be sent by the readinessProbe |
| readinessProbe.httpGet.path | string | `"/healthz"` | Path target for the HTTP GET request of the readinessProbe |
| readinessProbe.httpGet.port | string | `"http"` | Port target for the HTTP GET request of the readinessProbe |
| readinessProbe.httpGet.scheme | string | `"HTTP"` | Scheme to be used for the HTTP GET request of the readinessProbe |
| readinessProbe.initialDelaySeconds | int | `5` | Initial delay seconds for readinessProbe |
| readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| readinessProbe.timeoutSeconds | int | `5` | Timeout seconds for readinessProbe |
| redis.enabled | bool | `true` | Enable the Redis subchart |
| replicaCount | int | `2` | Amount of replicas for the ZAC-backend |
| resources | object | `{}` | Resources for zac-backend |
| secretReference.databasePass.secretKey | string | `"DB_PASSWORD"` | Name of the key which maps to the database password value |
| secretReference.databasePass.secretName | string | `"zac-secrets"` | Name of the referred secret which contains the database password |
| secretReference.databasePass.secretValue | string | `""` | OR (creates a new secret containing the database password value) |
| secretReference.elasticApmSecretToken.secretKey | string | `"ELASTIC_APM_SECRET_TOKEN"` | Name of the key which maps to elasticApmSecretToken |
| secretReference.elasticApmSecretToken.secretName | string | `"zac-secrets"` | Name of the referred secret which contains elasticApmSecretToken |
| secretReference.elasticApmSecretToken.secretValue | string | `""` | OR (creates a new secret containing elasticApmSecretToken) |
| secretReference.emailHostPassword.secretKey | string | `"EMAIL_HOST_PASSWORD"` | Name of the key which maps to emailHostPassword |
| secretReference.emailHostPassword.secretName | string | `"zac-secrets"` | Name of the referred secret which contains emailHostPassword |
| secretReference.emailHostPassword.secretValue | string | `""` | Name of the referred secret which contains emailHostPassword |
| secretReference.secretKey.secretKey | string | `"SECRET_KEY"` | Name of the key in the secret which maps to the secretKey value |
| secretReference.secretKey.secretName | string | `"zac-secrets"` | Name of the referred secret which contains the secretKey |
| secretReference.secretKey.secretValue | string | `""` | OR (creates a new secret containing the secretKey value) |
| secretReference.sentryDsn.secretKey | string | `"SENTRY_DSN"` | Name of the key which maps to Sentry's DSN |
| secretReference.sentryDsn.secretName | string | `"zac-secrets"` | Name of the referred secret which contains Sentry's DSN |
| secretReference.sentryDsn.secretValue | string | `""` | OR (creates a new secret containing Sentry's DSN) |
| securityContext | object | `{}` | Set zac-backend container security context |
| service.port | int | `8000` | zac-backend service port |
| service.type | string | `"ClusterIP"` | zac-backend service type |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `false` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| startupProbe.enabled | bool | `true` | Enable/disable the startupProbe |
| startupProbe.failureThreshold | int | `6` | Failure threshold for startupProbe |
| startupProbe.httpGet.httpHeaders | list | `[]` | Additional headers to be sent by the readinessProbe |
| startupProbe.httpGet.path | string | `"/healthz"` | Path target for the HTTP GET request of the startupProbe: |
| startupProbe.httpGet.port | string | `"http"` | Port target for the HTTP GET request of the startupProbe |
| startupProbe.httpGet.scheme | string | `"HTTP"` | Scheme to be used for the HTTP GET request of the startupProbe |
| startupProbe.initialDelaySeconds | int | `5` | Initial delay seconds for startupProbe |
| startupProbe.periodSeconds | int | `10` | Period seconds for startupProbe |
| startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| tolerations | list | `[]` | Tolerations for zac-backend pods assignment |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.0](https://github.com/norwoodj/helm-docs/releases/v1.11.0)
