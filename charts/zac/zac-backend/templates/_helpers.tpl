{{/*
Expand the name of the chart.
*/}}
{{- define "zac-backend.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "zac-backend.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "zac-backend.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "zac-backend.labels" -}}
helm.sh/chart: {{ include "zac-backend.chart" . }}
{{ include "zac-backend.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "zac-backend.selectorLabels" -}}
app.kubernetes.io/name: {{ include "zac-backend.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "zac-backend.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "zac-backend.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Return true if a secret object should be created
*/}}
{{- define "zac-backend.createSecret" -}}
{{- if .Values.secretReference.databasePass.secretValue -}}
    {{- true -}}
{{- else if .Values.secretReference.secretKey.secretValue -}}
    {{- true -}}
{{- else if .Values.secretReference.sentryDsn.secretValue -}}
    {{- true -}}
{{- else if .Values.secretReference.elasticApmSecretToken.secretValue -}}
    {{- true -}}
{{- else if .Values.secretReference.emailHostPassword.secretValue -}}
    {{- true -}}
{{- else -}}
{{- end -}}
{{- end -}}