# zac-frontend

![Version: 0.2.2](https://img.shields.io/badge/Version-0.2.2-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.44.0](https://img.shields.io/badge/AppVersion-0.44.0-informational?style=flat-square)

A Helm chart to deploy zaakafhandelcomponent frontend

**Homepage:** <https://github.com/GemeenteUtrecht/zaakafhandelcomponent>

## Installation

First configure the Helm repository:

```bash
helm repo add gemeente-utrecht https://gitlab.com/api/v4/projects/34434173/packages/helm/stable
helm repo update
```

Install the Helm chart with:

```bash
helm install zac-frontend gemeente-utrecht/zac-frontend \
    --set "settings.allowedHosts=zac-frontend.gemeente.nl" \
    --set "ingress.enabled=true" \
    --set "ingress.hosts=zac-frontend.gemeente.nl"

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for zac-backend pods assignment  |
| autoscaling.enabled | bool | `false` | Enable/disable autoscaling for the zac-backend deployment |
| autoscaling.maxReplicas | int | `100` | Maximum replicas for the zac-backend deployment |
| autoscaling.minReplicas | int | `1` | Minimum replicas for the zac-backend deployment |
| autoscaling.targetCPUUtilizationPercentage | int | `80` | zac-backend deployment autoscaling target CPU percentage |
| autoscaling.targetMemoryUtilizationPercentage | int | `80` | zac-backend deployment autoscaling target Mem utilization percentage |
| extraEnvs | list | `[]` | Additional environment variables to set |
| fullnameOverride | string | `""` | String to fully override zac-frontend.fullname |
| image.pullPolicy | string | `"Always"` | ZAC-frontend image pull policy |
| image.repository | string | `"scrumteamzgw/zaakafhandelcomponent"` | ZAC-frontend image repository |
| image.tag | string | `""` | Image tag override (whose default is the chart appVersion) |
| imagePullSecrets | list | `[]` | Image pull secrets |
| ingress.annotations | object | `{}` | Ingress annotations |
| ingress.className | string | `""` | Ingress Class which will be used to implement the Ingress |
| ingress.enabled | bool | `false` | Expose zac-frontend through an ingress |
| ingress.hosts[0] | object | `{"host":"chart-example.local","paths":[{"path":"/","pathType":"ImplementationSpecific"}]}` | Ingress hostname |
| ingress.hosts[0].paths[0] | object | `{"path":"/","pathType":"ImplementationSpecific"}` | Ingress path |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` | Ingress path type |
| ingress.tls | list | `[]` | Enable TLS for the Ingress |
| livenessProbe.enabled | bool | `true` | Enable/disable the livenessProbe |
| livenessProbe.failureThreshold | int | `2` | Failure threshold for livenessProbe |
| livenessProbe.httpGet.httpHeaders | list | `[]` | Additional headers to be sent by the livenessProbe |
| livenessProbe.httpGet.path | string | `"/healthz"` | Path target for the HTTP GET request of the livenessProbe |
| livenessProbe.httpGet.port | string | `"http"` | Port target for the HTTP GET request of the livenessProbe |
| livenessProbe.httpGet.scheme | string | `"HTTP"` | Scheme to be used for the HTTP GET request of the livenessProbe |
| livenessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for livenessProbe |
| livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| nameOverride | string | `""` | Name override for ZAC-frontend |
| nodeSelector | object | `{}` | Node labels for zac-backend pods assignment |
| podAnnotations | object | `{}` | Annotations for ZAC-frontend pods |
| podLabels | object | `{}` | Labels for ZAC-frontend pods |
| podSecurityContext | object | `{}` | Set zac-frontend pod security context |
| readinessProbe.enabled | bool | `true` | Enable/disable the readinessProbe |
| readinessProbe.failureThreshold | int | `6` | Failure threshold for readinessProbe |
| readinessProbe.httpGet.httpHeaders | list | `[]` | Additional headers to be sent by the readinessProbe |
| readinessProbe.httpGet.path | string | `"/healthz"` | Path target for the HTTP GET request of the readinessProbe |
| readinessProbe.httpGet.port | string | `"http"` | Port target for the HTTP GET request of the readinessProbe |
| readinessProbe.httpGet.scheme | string | `"HTTP"` | Scheme to be used for the HTTP GET request of the readinessProbe |
| readinessProbe.initialDelaySeconds | int | `5` | Initial delay seconds for readinessProbe |
| readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| readinessProbe.timeoutSeconds | int | `5` | Timeout seconds for readinessProbe |
| replicaCount | int | `1` | Amount of replicas for the ZAC-frontend |
| resources | object | `{}` | Resources for zac-backend |
| securityContext | object | `{}` | Set zac-frontend container security context |
| service.port | int | `8000` | zac-frontend service port |
| service.type | string | `"ClusterIP"` | zac-frontend service type |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `false` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| settings.alfresco.authURL | string | `""` | URL to Alfresco Authentication endpoint |
| settings.alfresco.previewURL | string | `""` | URL to Tezza  |
| settings.forms.url | string | `""` | URL to openformulieren |
| startupProbe.enabled | bool | `true` | Enable/disable the startupProbe |
| startupProbe.failureThreshold | int | `6` | Failure threshold for startupProbe |
| startupProbe.httpGet.httpHeaders | list | `[]` | Additional headers to be sent by the readinessProbe |
| startupProbe.httpGet.path | string | `"/healthz"` | Path target for the HTTP GET request of the startupProbe: |
| startupProbe.httpGet.port | string | `"http"` | Port target for the HTTP GET request of the startupProbe |
| startupProbe.httpGet.scheme | string | `"HTTP"` | Scheme to be used for the HTTP GET request of the startupProbe |
| startupProbe.initialDelaySeconds | int | `5` | Initial delay seconds for startupProbe |
| startupProbe.periodSeconds | int | `10` | Period seconds for startupProbe |
| startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| tolerations | list | `[]` | Tolerations for zac-backend pods assignment |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.0](https://github.com/norwoodj/helm-docs/releases/v1.11.0)
