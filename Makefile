ifdef CHART
CHARTARG=$(CHART)
else
$(error missing "CHART=<chartname>" argument)
endif

ifdef CHARTPATH
CHARTPATHARG=$(CHARTPATH)
else
CHARTPATHARG=charts
endif

define TEMPLATE
$(CHARTARG):
  extends: .helm-push
  variables:
    CHARTPATH: $(CHARTPATHARG)
    CHART: $(CHARTARG)
  rules:
    - if: $$CI_COMMIT_BRANCH == $$CI_DEFAULT_BRANCH
      changes:
        - "$${CHARTPATH}/$${CHART}/**/*"
endef

export TEMPLATE
create-pipeline:
	@echo "$$TEMPLATE" > .gitlab/$(CHART).yml